scriptdir=`dirname $0`
conf_file=$scriptdir/../src/main/resources/conf/target-sys.conf
echo $conf_file

vars="`set -o posix ; set`"; source $conf_file; other_vars="`grep -vFe "$vars" <<<"$(set -o posix ; set)" | grep -v ^vars=`"; unset vars;
other_vars=`echo $other_vars | sed 's/working_dir=[a-zA-Z0-9\/\-]*//'`
other_vars=`echo $other_vars | sed 's/ipc_dir=[a-zA-Z0-9\/\-]*//'`
other_vars=`echo $other_vars | sed 's/num_node=[0-9]*//'`

echo $other_vars
echo $working_dir

rm -r $working_dir/*
# 2> /dev/null
mkdir -p $working_dir

cp $scriptdir/reconfig3.sh $working_dir/reconfig3.sh
chmod 777 $working_dir/reconfig3.sh
cp $scriptdir/readconfig.sh $working_dir/readconfig.sh
chmod 777 $working_dir/readconfig.sh

cp $scriptdir/../src/main/resources/conf/target-sys.conf $working_dir/target-sys.conf
cp $scriptdir/resettest.sh $working_dir/resettest.sh
chmod 777 $working_dir/resettest.sh

echo traversal_record_dir=$working_dir/state >> $working_dir/target-sys.conf
echo test_record_dir=$working_dir/record >> $working_dir/target-sys.conf

mkdir $working_dir/record
mkdir $working_dir/state

# prepare ipc folders
rm -r $ipc_dir/*
# 2> /dev/null
mkdir -p $ipc_dir
mkdir $ipc_dir/new
mkdir $ipc_dir/send
mkdir $ipc_dir/ack
echo "IPC-File dirs for DMCK are ready in $ipc_dir"
