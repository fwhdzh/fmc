#!/usr/bin/env bash

. /home/fengwenhan/tmp/fmc/readconfig.sh

classpath=$target_sys_dir/build/classes
classpath=$classpath:$working_dir
lib=$target_sys_dir/build/lib
for k in `ls $lib/*.jar`; do
  echo $k
	classpath=$classpath:$k
done

export CLASSPATH=$CLASSPATH:$classpath:/home/fengwenhan/code/fmc-zookeeper-3.5.0/conf

echo $CLASSPATH

java -cp $CLASSPATH zkTest.ReconfigTwo
