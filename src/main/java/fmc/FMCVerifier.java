package fmc;

import fmc.transition.Transition;
import fmc.util.SpecVerifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FMCVerifier extends SpecVerifier {

    protected static final Logger log = LogManager.getLogger(FMCVerifier.class);

    @Override
    public boolean verify() {
        return true;
    }

    @Override
    public boolean verifyNextTransition(Transition transition) {
        return true;
    }
}
