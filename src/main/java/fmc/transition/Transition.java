package fmc.transition;

import java.io.Serializable;
import java.util.Comparator;

public abstract class Transition implements Serializable {

    public abstract int getTransitionId();
    public abstract boolean apply();

    public static final Comparator<Transition> COMPARATOR = new Comparator<Transition>() {
        public int compare(Transition o1, Transition o2) {
            Integer i1 = o1.getTransitionId();
            Integer i2 = o2.getTransitionId();
            return i1.compareTo(i2);
        }
    };
}