package fmc.util;

import fmc.server.ModelCheckingServerAbstract;
import fmc.transition.Transition;

public abstract class SpecVerifier {
    public ModelCheckingServerAbstract modelCheckingServer;

    public abstract boolean verify();
    public abstract boolean verifyNextTransition(Transition transition);
}
