package fmc.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/*
 * I use dir representing node that I have explored. If there is .finished under which dir,
 * it means that I have explored all possible path below that node
 * e.g. initDir = /tmp/record, /tmp/record/1/2/.finished means I have explored all possible
 * branch under 2 (i.e. 2/3/4/.., 2/4/3/..., ...). And /tmp/record/.finished means I am
 * finished exploring all possible branch
 */
public class FileSystemExploredBranchRecorder implements ExploredBranchRecorder {

    final static Logger LOG = LogManager.getLogger(FileSystemExploredBranchRecorder.class);

    int currentDepth;
    String initDir;
    String currentDir;

    public FileSystemExploredBranchRecorder(String initDir) {
        this.initDir = initDir;
        resetTraversal();
    }

    public void markBelowSubtreeFinished() {
        try {
            File finishedFile = new File(currentDir + "/.finished");
            finishedFile.createNewFile();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Marked " + currentDir + " to be explored");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isSubtreeBelowChildFinished(long child) {
        File finishedFlag = new File(currentDir + "/" + child + "/.finished");
        return finishedFlag.exists();
    }


    @Override
    public boolean createChild(long child) {
        File currentIdDirFile = new File(currentDir + "/" + child);
        if (!currentIdDirFile.exists()) {
            return currentIdDirFile.mkdir();
        }
        return true;
    }

    public boolean noteThisNode(String key, String value) {
        return noteThisNode(key, value.getBytes(), true);
    }

    public boolean noteThisNode(String key, String value, boolean overwrite) {
        return noteThisNode(key, value.getBytes(), overwrite);
    }

    public boolean noteThisNode(String key, byte[] value) {
        return noteThisNode(key, value, true);
    }

    public boolean noteThisNode(String key, byte[] value, boolean overwrite) {
        File note = new File(currentDir + "/" + key);
        try {
            if (!note.exists() || overwrite) {
                note.createNewFile();
                FileOutputStream fos = new FileOutputStream(note);
                BufferedOutputStream bos = new BufferedOutputStream(fos);
                bos.write(value);
                bos.close();
                fos.close();
            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void traverseUpward(int hop) {
        for (int i = 0; i < hop && currentDepth > 0; ++i, --currentDepth) {
            currentDir = currentDir.substring(0, currentDir.lastIndexOf('/'));
        }
    }

    @Override
    public void traverseDownTo(long child) {
        currentDir = currentDir + "/" + child;
        ++currentDepth;
    }

    /**
     * Initialize currentDir and currentDepth
     */
    public void resetTraversal() {
        currentDepth = 0;
        currentDir = initDir;
    }

    @Override
    public int getCurrentDepth() {
        return currentDepth;
    }

    public String getCurrentPath() {
        return currentDir;
    }
}
