package fmc.util;

public interface ExploredBranchRecorder {
    public boolean isSubtreeBelowChildFinished(long child);
    public boolean createChild(long child);
    public boolean noteThisNode(String key, String value);
    public int getCurrentDepth();
    public void markBelowSubtreeFinished();
    public void traverseUpward(int hop);
    public void traverseDownTo(long child);
    public void resetTraversal();
    public String getCurrentPath();
}
