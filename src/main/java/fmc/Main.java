package fmc;


import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
//import java.util.logging.Logger;

public class Main {

//    private static final Logger logger = LogManager.getLogger("Main");



    public static String target_sys_dir = "";
    public static String working_dir = "";

    public void prepareWorkload(){
//        InputStream in = this.getClass().getResourceAsStream("conf/target-sys.json");



        try {

//            File confFile = new File("E:\\javafile\\fmc\\src\\main\\resources\\conf\\target-sys.json");
            File confFile = new File("/home/fwhdzh/code/fmc/src/main/resources/conf/target-sys.json");

            String conf = FileUtils.readFileToString(confFile,StandardCharsets.UTF_8);
            JSONObject jsonObject = JSONObject.parseObject(conf);
            target_sys_dir = jsonObject.getString("target_sys_dir");
            working_dir = jsonObject.getString("working_dir");

            Runtime.getRuntime().exec("cp -rf "+ target_sys_dir + " " + working_dir);

            System.out.println(target_sys_dir);

//            Logger logger = LoggerFactory
//            Logger logger = LoggerFactory.getLogger(Main.class);
//            logger.error(target_sys_dir);
//            logger.info(working_dir);

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }


    }

    public static void main(String[] args) {
        System.out.println("Welcome to FMC!");

        Main m = new Main();
        m.prepareWorkload();
    }
}
