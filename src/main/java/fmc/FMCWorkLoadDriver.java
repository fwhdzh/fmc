package fmc;

import fmc.util.WorkloadDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class FMCWorkLoadDriver extends WorkloadDriver {

    final static Logger LOG = LogManager.getLogger(FMCWorkLoadDriver.class);

    String ipcDir;
    Process[] node;

    public FMCWorkLoadDriver(int numNode, String workingDir, String sIpcDir, String samcDir, String targetSysDir) {
        super(numNode, workingDir, sIpcDir, samcDir, targetSysDir);
        ipcDir = sIpcDir;
        node = new Process[numNode+1];
    }

    public void resetTest(int testId) {
        this.testId = testId;
        try{
            LOG.info("exec:"+workingDir + "/resettest.sh " + workingDir + " " + numNode + " " + testId);
            Runtime.getRuntime().exec(workingDir + "/resettest.sh " + workingDir + " " + numNode + " " + testId);
        } catch (Exception e) {
            LOG.error("Error in creating new directory in log");
            e.printStackTrace();
        }
    }

    @Override
    public void runWorkload() {

    }

    /**
     * This method call startNode for all the node we will use.
     * Noted!!! This method is a little different from SAMC.
     */
    public void startEnsemble() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Starting ensemble");
        }
        try{
//            for(int i=0; i<numNode; i++){
//                resetDynamic(i);
//            }

            startNode(1);
            Thread.sleep(200);

            startNode(2);
            Thread.sleep(200);
            //reconfig(1);

            startNode(3);
            Thread.sleep(200);
//            reconfig(3);

        } catch (InterruptedException e) {
            LOG.error("Error in starting node");
        }
    }

    public void reconfig(int id){
        if (LOG.isDebugEnabled()) {
            LOG.debug("Reconfig node " + id);
        }
        Process process = null;
        BufferedReader bufrIn = null;
        BufferedReader bufrError = null;
        try {
            LOG.info("exec:" +workingDir + "/reconfig" + id + ".sh");

            process = Runtime.getRuntime().exec(workingDir + "/reconfig" + id + ".sh");
//            process.waitFor();
//            Thread.sleep(500);

//            Thread a = new Thread(){
//                @Override
//                public void run() {
//                    super.run();
//
//                }
//            };
//            a.run();
            bufrIn = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
            bufrError = new BufferedReader(new InputStreamReader(process.getErrorStream(), "UTF-8"));

            StringBuilder result = new StringBuilder();

            String line = null;
            while ((line = bufrIn.readLine()) != null) {
//                result.append(line).append('\n');
                LOG.info(line);
            }
            while ((line = bufrError.readLine()) != null) {
//                result.append(line).append('\n');
                LOG.info(line);
            }

            LOG.info(result.toString());

            if (bufrIn != null){
                bufrIn.close();
            }
            if (bufrError != null){
                bufrError.close();
            }
            if (process != null) {
                process.destroy();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void stopEnsemble() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Stopping ensemble");
        }
        for (int i=1; i<numNode+1; i++) {
            try {
                stopNode(i);
                Thread.sleep(200);
            } catch (InterruptedException e) {
                LOG.error("Error in stopping node " + i);
            }
        }

        if(ipcDir != ""){
            cleanUpIPCDir();
        }
    }

    public void startNode(int id) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Starting node " + id);
        }
        System.out.println("Starting node " + id);
        try {
//            node[id] = Runtime.getRuntime().exec(workingDir + "/startNode.sh " + workingDir + " " +
//                    targetSysDir + " " + id + " " + testId);
            LOG.info(targetSysDir + "/bin/zkServer_"+id+".sh start " +
                    targetSysDir + "/conf/zoo_"+ (id) + ".cfg");
            node[id] = Runtime.getRuntime().exec(targetSysDir + "/bin/zkServer_"+id+".sh start " +
                    targetSysDir + "/conf/zoo_"+ (id) + ".cfg");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void stopNode(int id) {
        LOG.info("Stopping node " + id);
        try {
            Runtime.getRuntime().exec(targetSysDir + "/bin/zkServer_"+id+".sh stop " +
                    targetSysDir + "/conf/zoo_"+ (id) + ".cfg");
//            Runtime.getRuntime().exec(workingDir + "/killNode.sh " + workingDir + " " + id + " " + testId);
            Thread.sleep(10);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void cleanUpIPCDir(){
        try {
            Runtime.getRuntime().exec(new String[]{"sh","-c", "rm " + ipcDir + "/new/*"});
            Runtime.getRuntime().exec(new String[]{"sh","-c", "rm " + ipcDir + "/send/*"});
            Runtime.getRuntime().exec(new String[]{"sh","-c", "rm " + ipcDir + "/ack/*"});

            System.out.println("Finished cleaning up");
        } catch (IOException e) {
            LOG.error("Error in cleaning up ipcDir");
        }
    }
}
