package fmc.server;

import fmc.transition.PacketSendTransition;
import fmc.transition.Transition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

abstract class InstructionTransition {

    abstract Transition getRealTransition(ModelCheckingServerAbstract checker);

}

class PacketSendInstructionTransition extends InstructionTransition {

    protected final Logger LOG = LogManager.getLogger(this.getClass());;

    long packetId;

    public PacketSendInstructionTransition(long packetId) {
        this.packetId = packetId;
    }

    @Override
    Transition getRealTransition(ModelCheckingServerAbstract checker) {
        if (packetId == 0) {
            return (Transition) checker.currentEnabledTransitions.peekFirst();
        }
        for (int i = 0; i < 25; ++i) {
            for (Object t : checker.currentEnabledTransitions) {
                if(t instanceof PacketSendTransition){
                    PacketSendTransition p = (PacketSendTransition) t;
                    if (p.getTransitionId() == packetId) {
                        return p;
                    }
                }
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                LOG.error("", e);
            }
//            checker.getOutstandingTcpPacketTransition(queue);
            checker.updateFMCQueue();
        }
        throw new RuntimeException("No expected enabled packet for " + packetId);
    }

}