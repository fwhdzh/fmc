package fmc.server;

import fmc.FMCVerifier;
import fmc.TestRunner;
import fmc.event.Event;
import fmc.transition.NodeCrashTransition;
import fmc.transition.PacketSendTransition;
import fmc.transition.Transition;
import fmc.util.SpecVerifier;
import fmc.util.WorkloadDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class ModelCheckingServerAbstract implements ModelCheckingServer{

    private static String PATH_FILE = "path";

    final static Logger LOG = LogManager.getLogger(ModelCheckingServerAbstract.class);

    public int numNode;

    public boolean[] isNodeOnline;

    protected int testId;

    protected WorkloadDriver workloadDriver;
    protected SpecVerifier verifier;

    protected LinkedList<Transition> currentEnabledTransitions = new LinkedList<Transition>();
    /** Array to store if the nodes in target-sys is steady */
    protected boolean[] isNodeSteady;
    protected Boolean isStarted = false;

    /** The thread to do most of logic of model checking, such as check if finished,
     * get the packet that will be handled, etc.
     * This variable is instantiated in the subclass.
     */
    protected Thread modelChecking;

    /**
     * In SAMC, it is seems this member variable is never used.
     */
    protected int[] numPacketSentToId;

    protected String testRecordDirPath;
    protected String idRecordDirPath;
    protected String pathRecordFilePath;
    protected FileOutputStream pathRecordFile;

    protected LinkedList<String> initialPath = new LinkedList<String>();
    protected int initialPathCounter;
    protected boolean hasInitialPath;
    protected boolean hasFinishedInitialPath;

    protected int steadyStateTimeout = 500;
    protected int initSteadyStateTimeout = 500;
    protected int waitEndExploration = 3000;

    /**
     * For a certain fromId(identify the node who send the event) and
     * a certain toId, messagesQueue[fromId][toId] is a queue that contains
     * all the event from the node of fromId and to the node of toId.
     */
    protected ConcurrentLinkedQueue<Event>[][] messagesQueues;

    protected String ipcDir;

    public ModelCheckingServerAbstract(String interceptorName, String ackName, int numNode,
                                       String testRecordDirPath, String workingDirPath, WorkloadDriver workloadDriver,
                                       String ipcDir) {
        this.numNode = numNode;
        this.testRecordDirPath = testRecordDirPath;

        isNodeOnline = new boolean[numNode+1];

        /**
         * use numNode+1 because the zookeeper myid starts with 1 and we must
         * make messagesQueues size is numNode+1 to avoid ArrayIndexOutOfBoundsException.
         */
        messagesQueues = new ConcurrentLinkedQueue[numNode+1][numNode+1];

        this.workloadDriver = workloadDriver;
        this.verifier = workloadDriver.verifier;

        pathRecordFile = null;

        this.ipcDir = ipcDir;

        this.resetTest();
    }

    /**
     * Add a Event(we can also call it a message or packet, however its
     * class name in our code is Event) to messagesQueue
     * In messageQueue, these events will be handled one by one.
     * @param event the event will be added to messagesQueue.
     */
    public void offerPacket(Event event){
        messagesQueues[event.getFromId()][event.getToId()].add(event);
        LOG.info("Intercept event " + event.toString() +" to messagesQueue");
    }

    /**
     * The method fmc to control packet queue, crash an reboot and some
     * other things. To put it another way, this method make fmc to next state.
     */
    public void updateFMCQueue(){
        getOutstandingTcpPacketTransition(currentEnabledTransitions);

//        adjustCrashAndReboot(currentEnabledTransitions);
//        getOutstandingSnapshot(currentEnabledTransitions);

        printTransitionQueues(currentEnabledTransitions);
    }

    public void updateFMCQueueAfterEventExecution(Transition transition){
        if (transition instanceof NodeCrashTransition) {

        }
    }

    /**
     * These method do the following things:
     * 1. Add all the messages that in messagesQueues but there is no message from
     * the node which this message sends from and to the node which this message sends
     * to to a buffer. (Be careful because in this process, only the head event of a
     * messageQueue will be added to the offer.)
     * 2. reorder the buffer By the event.id.
     * 3. add all the events in buffer to transitionList.(so the transitionList will be partial order)
     * 4. Add the message from local events to transitionList
     * @param transitionList the transitionList will contains the new messages after this method
     */
    public void getOutstandingTcpPacketTransition(LinkedList<Transition> transitionList) {
        /**
         * Add all the messages that in messagesQueues but there is no message from
         * the node which this message sends from and to the node which this message sends
         * to to transitionList.
         */
        boolean[][] filter = new boolean[numNode+1][numNode+1];
        for (int i = 1; i <= numNode; ++i) {
            Arrays.fill(filter[i], true);
        }
        for (Transition t : transitionList) {
            if (t instanceof PacketSendTransition) {
                PacketSendTransition p = (PacketSendTransition) t;
                filter[p.getPacket().getFromId()][p.getPacket().getToId()] = false;
            }
        }
        LinkedList<PacketSendTransition> buffer = new LinkedList<PacketSendTransition>();
        for (int i = 1; i <= numNode; ++i) {
            for (int j = 1; j <= numNode; ++j) {
                /**
                 * Be careful, in this logic, only the head event of a
                 * messageQueue will be added to the offer.
                 */
                if (filter[i][j] && !messagesQueues[i][j].isEmpty()) {
                    buffer.add(new PacketSendTransition(this, messagesQueues[i][j].remove()));
                }
            }
        }

        // reorder the buffer By the event.id.
        Collections.sort(buffer, new Comparator<PacketSendTransition>() {
            public int compare(PacketSendTransition o1, PacketSendTransition o2) {
                Integer i1 = o1.getPacket().getId();
                Integer i2 = o2.getPacket().getId();
                return i1.compareTo(i2);
            }
        });

        /**
         * add all the events in buffer to transitionList.
         * (so the transitionList will be partial order)
         */
        transitionList.addAll(buffer);

        // Add the message from local events to transitionList
        getLocalEvents(transitionList);
    }

    public void setInitialPath(String initialPath){
        this.hasInitialPath = !initialPath.isEmpty();
        this.hasFinishedInitialPath = !hasInitialPath;
        if(hasInitialPath){
            LOG.info("InitialPath: " + initialPath);
            readInitialPath(initialPath);
        }
    }

    public void readInitialPath(String initialPath){
        // read file from initialPath file
        try{
            BufferedReader initialPathReader = new BufferedReader(new FileReader(initialPath));
            String line;
            while ((line = initialPathReader.readLine()) != null){
                this.initialPath.add(line);
            }
            initialPathReader.close();
        } catch (Exception e){
            LOG.error("Error in readInitialPath");
        }
    }


    public void getLocalEvents(LinkedList<Transition> transitionList){

    }

    public void printTransitionQueues(LinkedList<Transition> transitionList){
        StringBuilder sb = new StringBuilder();
        sb.append("-----------------------------\n");
        sb.append("Events in DMCK Queue : " + transitionList.size() + "\n");

        int counter = 1;
        for (Transition t : transitionList) {
            if(t != null){
                sb.append(counter + ". " + t.toString()+"\n");
            } else {
                sb.append(counter + ". null event"+"\n");
            }
            counter++;
        }
        sb.append("-----------------------------\n");
        LOG.info(sb.toString());
    }

    public void setTestId(int testId) {
        LOG.info("This test has id = " + testId);
        this.testId = testId;
        idRecordDirPath = testRecordDirPath + "/" + testId;
        File testRecordDir = new File(idRecordDirPath);
        if (!testRecordDir.exists()) {
            testRecordDir.mkdir();
        }
        pathRecordFilePath = idRecordDirPath + "/" + PATH_FILE;
        /**
         * This method has not been not completely copied, it will be
         * copied in the future.
         */
    }

    protected void waitNodeSteady(int id) throws InterruptedException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Waiting node " + id + " to be in steady state");
        }

        int timeoutCounter = 0;
        int timeoutFraction = 20;
        while (!isNodeSteady(id) && timeoutCounter <= timeoutFraction) {
            Thread.sleep(steadyStateTimeout/timeoutFraction);
            timeoutCounter++;
        }

        if(timeoutCounter >= timeoutFraction){
            LOG.warn("Steady state for node " + id + " triggered by timeout");
        }

        setNodeSteady(id, true);
    }

    /**
     * This method will first set the nodeSteady(the aim node of the given packet) to false. And then
     * call method "commit" to make FMC-Zookeeper enable this packet. Whether success or fail, the
     * nodeSteady will return to false after this method.
     * @param packet the packet we want to commit(make it enable) to FMC-Zookeeper
     * @return if success, return true. if else, return fasle.
     * @throws InterruptedException
     */
    public boolean commitAndWait(Event packet) throws InterruptedException {
        setNodeSteady(packet.getToId(), false);
        boolean result = false;
        if (commit(packet)) {
            result = true;
        }
        if (result) {
            waitNodeSteady(packet.getToId());
            return true;
        } else {
            setNodeSteady(packet.getToId(), true);
            return false;
        }
    }

    /**
     * Do some necessary operation before a new test begin
     * include create new messageQueue, initialize ls, gs, etc.
     */
    public void resetTest() {

        testId = -1;

        Arrays.fill(isNodeOnline, true);

        initialPathCounter = 0;
        hasFinishedInitialPath = !hasInitialPath;

        /**
         * use numNode+1 because the zookeeper myid starts with 1 and we must
         * make messagesQueues size is numNode+1 to avoid ArrayIndexOutOfBoundsException.
         */
        messagesQueues = new ConcurrentLinkedQueue[numNode+1][numNode+1];
        for (int i = 0; i < numNode +1; ++i) {
            for (int j = 0; j < numNode +1; ++j) {
                messagesQueues[i][j] = new ConcurrentLinkedQueue<Event>();
            }
        }

        isNodeSteady = new boolean[numNode+1];
        isStarted = false;

        numPacketSentToId = new int[numNode+1];
    }

    /**
     * This method will find the nextInitialTransition from currentEnabledTransition.
     * <br>
     * If the nextInitialTransition is not in currentEnabledTransition now, the system
     * will wait it for some time. And if still not, it will throw a exception. These
     * logic all written in PacketSendInstructionTransition.getRealTransition and called
     * by this method.
     * <br>
     * For now, this method is different from samc and it can only handle
     * PacketSendInstructionTransition.
     * @return the nextInitialTransition
     */
    protected Transition nextInitialTransition(){
        InstructionTransition instruction;
        String[] tokens = initialPath.get(initialPathCounter).split(" ");
        initialPathCounter++;
        if(initialPathCounter >= initialPath.size()){
            hasFinishedInitialPath = true;
        }
        if (tokens[0].equals("packetsend")) {
            String packetTransitionIdString = tokens[1].split("=")[1];
            if (packetTransitionIdString.equals("*")) {
                instruction = new PacketSendInstructionTransition(0);
            } else {
                long packetTransitionId = Long.parseLong(packetTransitionIdString);
                instruction = new PacketSendInstructionTransition(packetTransitionId);
            }
        } else {
            return null;
        }

        Transition transition = instruction.getRealTransition(this);
        int id = -1;
        for(int i=0; i<currentEnabledTransitions.size(); i++){
            // replace abstract with real one based on id
            Transition eventInQueue = currentEnabledTransitions.get(i);

            if(transition.getTransitionId() == eventInQueue.getTransitionId()){
                id = i;
                break;
            }
        }
        return currentEnabledTransitions.remove(id);
    }

    public void updateGlobalState() {

    }

    /**
     * This method is different from SAMC and it lacks much logic.
     * For now, it only has the ability to initialize pathRecordFile.
     */
    protected void initGlobalState() {
        try {
            pathRecordFile = new FileOutputStream(pathRecordFilePath);
        }catch (FileNotFoundException e) {
            LOG.error("", e);
        }

    }


    public boolean runEnsemble() {
        for (int i = 1; i < numNode+1; ++i) {
            setNodeOnline(i, true);
        }
        workloadDriver.startEnsemble();
        return true;
    }

    /**
     * Stop All zookeeper nodes which is running and clear the messagesQueues
     * @return if this method success, return true, else false
     */
    public boolean stopEnsemble() {
        workloadDriver.stopEnsemble();
        for (int i = 1; i < numNode + 1; ++i) {
            setNodeOnline(i, false);
            for (int j = 1; j < numNode + 1; ++j) {
                messagesQueues[i][j].clear();
                messagesQueues[j][i].clear();
            }
        }
        return true;
    }

    public void setNodeOnline(int id, boolean isOnline) {
        isNodeOnline[id] = isOnline;
    }

    public boolean isNodeOnline(int id) {
        return isNodeOnline[id];
    }

    protected void setNodeSteady(int id, boolean isSteady) {
        isNodeSteady[id] = isSteady;
    }

    /**
     * figure if a node is steady, it will return true while isNodeSteady[] is
     * true or inNodeOnline[] is false
     * @param id id of the node
     * @return return true while isNodeSteady[] is true or inNodeOnline[] is false
     */
    protected boolean isNodeSteady(long id) {
        return isNodeSteady[(int) id] || !isNodeOnline[(int) id];
    }

    /**
     * This method will write given packet information to ipcDir/new directory, and then move it
     * to ipcDir/ack directory. The files in ipcDir/ack directory will be collected by FMC-Zookeeper.
     * (Put it another way, we actually tell FMC-Zookeeper to enable the given packet.) In last, we will
     * update numPacketSentToId to record this operation
     * @param packet The packet we will write to ipc folder(to enable)
     * @return if all the operations have been done, return true. if else, return false.
     */
    public boolean commit(Event packet) {
        boolean result;
        try {
            try{
                PrintWriter writer = new PrintWriter(ipcDir + "/new/" + packet.getValue(Event.FILENAME), "UTF-8");
                writer.println("eventId=" + packet.getId());
                writer.println("execute=true");
                writer.close();

                LOG.info("Enable event with ID : " + packet.getId() + " filename: " + packet.getValue(Event.FILENAME));

                Runtime.getRuntime().exec("mv " + ipcDir + "/new/" + packet.getValue(Event.FILENAME) + " " +
                        ipcDir + "/ack/" + packet.getValue(Event.FILENAME));
            } catch (Exception e) {
                LOG.error("Error in creating new ack file : " + packet.getValue(Event.FILENAME));
            }

            result = true;
        } catch (Exception e) {
            LOG.warn("There is an error when committing this packet, " + packet.toString());
            result = false;
        }
        if (result) {
            synchronized (numPacketSentToId) {
                numPacketSentToId[packet.getToId()]++;
            }
            return true;
        }
        return false;
    }

    /**
     * figure if all the nodes in the target system are steady.
     * @return true if all the nodes in the target system are steady, either false.
     */
    protected boolean isSystemSteady() {
        for (int i = 1; i < numNode + 1; ++i) {
            if (!isNodeSteady(i)) {
                return false;
            }
        }
        return true;
    }

    public void informSteadyState(int id, int runningState) throws RemoteException {
        setNodeSteady(id, true);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Node " + id + " is in steady state");
        }

    }

    public void waitOnSteadyStatesByTimeout(){
        LOG.info("Starts wait on first steady states");
        try{
            Thread.sleep(initSteadyStateTimeout);
            for(int i=1; i<numNode + 1; i++){

                informSteadyState(i, 0);
                /**
                 * in SAMC this block is in informSteadyStare method.
                 * I think it will be better to place this method or be an call function
                 * which declare as a separate method.
                 */
                synchronized (isStarted) {
                    if (!isStarted && isSystemSteady()) {
                        isStarted = true;
                        initGlobalState();
                        LOG.info("First system steady state, start model checker thread.");
                        modelChecking.start();
                    }
                }
            }
        } catch (Exception e){
            e.printStackTrace();
            LOG.error("Error while waiting on the first steady states timeout");
        }
    }
}
