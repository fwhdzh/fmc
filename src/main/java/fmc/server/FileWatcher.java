package fmc.server;

import fmc.event.Event;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class FileWatcher implements Runnable {
    private final static Logger LOG = LogManager.getLogger(FileWatcher.class);

    String ipcDir;
    File path;
    ModelCheckingServerAbstract checker;

    /**
     * the hashtable of each packet and its id
     * (the id will be used in fmc as an unique identification)
     */
    private HashMap<Integer, Integer> packetCount;

    public FileWatcher(String sPath, ModelCheckingServerAbstract modelChecker) {
        ipcDir = sPath;
        path = new File(sPath + "/send");
        checker = modelChecker;
        resetPacketCount();

        if (!path.isDirectory()) {
            throw new IllegalArgumentException("Path: " + path + " is not a folder");
        }
    }

    public void run() {
        LOG.info("FileWatcher is looking after: " + path);

        while (!Thread.interrupted()) {
            if (path.listFiles().length > 0) {
                for (File file : path.listFiles()) {
                    processNewFile(file.getName());
                }
            }
            try {
                Thread.sleep(25);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    public void resetPacketCount() {
        packetCount = new HashMap<Integer, Integer>();
    }

    public synchronized void processNewFile(String filename) {
        try {
            Properties ev = new Properties();
            FileInputStream evInputStream = new FileInputStream(path + "/" + filename);
            ev.load(evInputStream);

            if (filename.startsWith("zk-")) {
                int sender = Integer.parseInt(ev.getProperty("sender"));
                int recv = Integer.parseInt(ev.getProperty("recv"));
                int state = Integer.parseInt(ev.getProperty("state"));
                long leader = Long.parseLong(ev.getProperty("leader"));
                long zxid = Long.parseLong(ev.getProperty("zxid"));
                long epoch = Long.parseLong(ev.getProperty("epoch"));
                int eventId = (int) Long.parseLong(filename.substring(3));
                int hashId = commonHashId(eventId);

                LOG.info("DMCK receives ZK event with hashId-" + hashId + " sender-" + sender +
                        " recv-" + recv + " state-" + state + " leader-" + leader +
                        " zxid-" + zxid + " epoch-" + epoch + " filename-" + filename);

                Event event = new Event(hashId);
                event.addKeyValue(Event.FROM_ID, sender);
                event.addKeyValue(Event.TO_ID, recv);
                event.addKeyValue(Event.FILENAME, filename);
                event.addKeyValue("state", state);
                event.addKeyValue("leader", leader);
                event.addKeyValue("zxid", zxid);
                event.addKeyValue("epoch", epoch);

                checker.offerPacket(event);
            } else if (filename.startsWith("sync-")) {
                int sendNode = Integer.parseInt(ev.getProperty("sender"));
                int recvNode = Integer.parseInt(ev.getProperty("recv"));
                String strRole = ev.getProperty("strSendRole");
                int leader = Integer.parseInt(ev.getProperty("leader"));
                long zxid = Long.parseLong(ev.getProperty("zxid"));
                int eventId = Integer.parseInt(filename.substring(5));
                int hashId = commonHashId(eventId);

                Event event = new Event(hashId);
                event.addKeyValue(Event.FROM_ID, sendNode);
                event.addKeyValue(Event.TO_ID, recvNode);
                event.addKeyValue(Event.FILENAME, filename);
                event.addKeyValue("senderRole", strRole);
                event.addKeyValue("leader", leader);
                event.addKeyValue("zxid", zxid);

                LOG.info("DMCK receives ZK sync File : eventId-" + filename.substring(5) +
                        " sendNode-" + sendNode + " sendRole-" + ev.getProperty("sendRole") +
                        " recvNode-" + ev.getProperty("recvNode") + " leader-" + ev.getProperty("leader"));

                checker.offerPacket(event);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // remove the received msg
        LOG.debug("DMCK deletes " + path + "/" + filename);
        try {
            Runtime.getRuntime().exec("rm " + path + "/" + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * give an eventId(this eventId is figure out in fmc-zookeeper, it
     * is an unique identification), and return a hash Id.
     *
     * @param eventId the eventId is figure out in fmc-zookeeper, it is an unique identification
     * @return the hash id of the eventId
     */
    private int commonHashId(int eventId) {
        Integer count = packetCount.get(eventId);
        if (count == null) {
            count = 0;
        }
        count++;
        packetCount.put(eventId, count);
        return 31 * eventId + count;
    }
}
