package fmc.server;

import com.alibaba.fastjson.JSON;
import fmc.transition.Transition;
import fmc.util.ExploredBranchRecorder;
import fmc.util.FileSystemExploredBranchRecorder;
import fmc.util.WorkloadDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ListIterator;

public class FMCModelChecker extends ModelCheckingServerAbstract{

    private final static Logger LOG = LogManager.getLogger(FMCModelChecker.class);

    protected String stateDir;
    protected ExploredBranchRecorder exploredBranchRecorder;

    public FMCModelChecker(String interceptorName, String ackName, int numNode,
                           int numCrash, int numReboot, String globalStatePathDir, String packetRecordDir,
                           String workingDir, WorkloadDriver workloadDriver, String ipcDir){
        super(interceptorName, ackName, numNode, globalStatePathDir, workingDir, workloadDriver,
                ipcDir);
        this.stateDir = packetRecordDir;
        exploredBranchRecorder = new FileSystemExploredBranchRecorder(packetRecordDir);
        resetTest();
    }

    public Transition nextTransition(LinkedList<Transition> transitions){
        LOG.info(exploredBranchRecorder.getCurrentPath());
        ListIterator<Transition> iter = transitions.listIterator();
        while (iter.hasNext()) {
            Transition transition = iter.next();
            if (!exploredBranchRecorder.isSubtreeBelowChildFinished(transition.getTransitionId())) {
                iter.remove();
                return transition;
            }
        }

        return null;
    }

    @Override
    public void resetTest() {
        /**
         * This if() {return} is only for a question about "how to make a subclass
         * instance to call parent class method in parent class constructor？"
         * if we didn't put this statement, it will throe NullPointException since
         * the ModelCheckingServerAbstract.<init> will call FMCModelChecker.resetTest
         * instead of ModelCheckingServerAbstract.resetTest.
         */
        if (exploredBranchRecorder == null) {
            return;
        }
        super.resetTest();
        modelChecking = new PathTraversalWorker();
        currentEnabledTransitions = new LinkedList<Transition>();
        if (exploredBranchRecorder == null){
            LOG.info("exploredBranchRecorder is null");
        }
        exploredBranchRecorder.resetTraversal();
        File waiting = new File(stateDir + "/.waiting");
        try {
            waiting.createNewFile();
        } catch (IOException e) {
            LOG.error("", e);
        }
//        currentCrash = 0;
//        currentReboot = 0;
    }

    protected void recordTestId() {
        exploredBranchRecorder.noteThisNode(".test_id", testId + "");
    }

    protected void saveFinishedPath() {
        return;
    }

    class PathTraversalWorker extends Thread{
        @Override
        public void run() {

            boolean hasExploredAll = false;
            boolean hasWaited = false;
            LinkedList<LinkedList<Transition>> pastEnabledTransitionList =
                    new LinkedList<LinkedList<Transition>>();

            while (true) {
                updateFMCQueue();
                boolean terminationPoint = currentEnabledTransitions.isEmpty();

                if (terminationPoint && hasWaited){
                    boolean verifiedResult = verifier.verify();
                    recordTestId();
                    exploredBranchRecorder.markBelowSubtreeFinished();
                    saveFinishedPath();

                    for (LinkedList<Transition> pastTransitions : pastEnabledTransitionList) {
                        exploredBranchRecorder.traverseUpward(1);
                        Transition transition = nextTransition(pastTransitions);
                        if (transition == null) {
                            exploredBranchRecorder.markBelowSubtreeFinished();
                            saveFinishedPath();
                            hasExploredAll = true;
                        } else {
                            hasExploredAll = false;
                            break;
                        }
                    }
                    LOG.info("---- End of Path Execution ----");
                    if(!hasExploredAll){
                        resetTest();
                        break;
                    }
                } else if(terminationPoint){
                    try {
                        hasWaited = true;
                        LOG.debug("Wait for any long process");
                        Thread.sleep(waitEndExploration);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    continue;
                }

                pastEnabledTransitionList.addFirst((LinkedList<Transition>) currentEnabledTransitions.clone());

//                LOG.info("[currentEnabledTransitions] " + JSON.toJSONString(currentEnabledTransitions));
//                LOG.info("[PastEnabledTransitionList] " + JSON.toJSONString(pastEnabledTransitionList));

                Transition transition;

                boolean recordPath = true;
                if(hasInitialPath && !hasFinishedInitialPath){
                    transition = nextInitialTransition();
                    LOG.info("Next transition is directed by initialPath");
                    recordPath = false;
                } else {
                    transition = nextTransition(currentEnabledTransitions);
                }

                if (transition != null) {



                    if(recordPath){
                        exploredBranchRecorder.createChild(transition.getTransitionId());
                        exploredBranchRecorder.traverseDownTo(transition.getTransitionId());
                        exploredBranchRecorder.noteThisNode(".packet", transition.toString());
                    }

                    boolean cleanTransition = verifier.verifyNextTransition(transition);

                    if(!cleanTransition){

                    }
                    LOG.info("[NEXT TRANSITION] " + transition.toString());

                    try {
                        if (transition.apply()) {

                            pathRecordFile.write((transition.toString() + "\n").getBytes());

                            updateGlobalState();
                            updateFMCQueueAfterEventExecution(transition);
                        }
                    } catch (Exception e) {
                        LOG.error("", e);
                    }

                }   else if (exploredBranchRecorder.getCurrentDepth() == 0) {
                    LOG.warn("Finished exploring all states");
                     workloadDriver.stopEnsemble();
                    System.exit(0);

                }   else {

                    if(!hasWaited){
                        /**
                         * in samc, the code there is different. And I can't determine
                         * if the code is for raft or for all the target system. So I
                         * write this code in my own way.
                         */
                        try {
                            hasWaited = true;
                            LOG.debug("Wait for any long process");
                            Thread.sleep(waitEndExploration);
                        } catch (InterruptedException e) {
                            LOG.error("", e);
                        }
                        continue;
                    } else {
                        LOG.error("There might be some errors");
                        workloadDriver.stopEnsemble();
                        System.exit(1);
                    }
                }

                hasWaited = false;

            }
        }
    }


}
