package fmc;


import fmc.server.FMCModelChecker;
import fmc.server.FileWatcher;
import fmc.server.ModelCheckingServerAbstract;
import fmc.util.SpecVerifier;
import fmc.util.WorkloadDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Properties;

public class TestRunner {
    final static Logger LOG = LogManager.getLogger(TestRunner.class);

    static WorkloadDriver workloadDriver;

    public static void main(String[] args) {
        LOG.info("f.w.h");

        String configFileLocation = TestRunner.class.getResource("/conf/target-sys.conf").getFile();
        prepareModelChecker(configFileLocation,false);

    }

    /**
     *
     * @param configFileLocation This is usually the target-sys.conf in our project folder, it is
     *                           different from the confFile used in createModelCheckerFromConf
     * @param pauseEveryPathExploration
     */
    public static void prepareModelChecker(String configFileLocation, boolean pauseEveryPathExploration) {
        Properties config = new Properties();
        FileInputStream configInputStream = null;

        try {
            configInputStream = new FileInputStream(configFileLocation);
            config.load(configInputStream);
            configInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        String workingDir = config.getProperty("working_dir");
        String ipcDir = config.getProperty("ipc_dir");
        String samcDir = config.getProperty("samc_dir");
        String targetSysDir = config.getProperty("target_sys_dir");
        String workload = config.getProperty("workload_driver");
        int numNode = Integer.parseInt(config.getProperty("num_node"));

        workloadDriver = new FMCWorkLoadDriver(numNode, workingDir, ipcDir, samcDir, targetSysDir);
        ModelCheckingServerAbstract checker = createModelCheckerFromConf(workingDir + "/target-sys.conf", workingDir, workloadDriver, ipcDir);

        // Maybe we can move this call to main method
        try {
            startExploreTesting(checker,3,workingDir,pauseEveryPathExploration,ipcDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param confFile this confFile is in workingDir, it is different from the configFileLocation used
     *                 in prepareModelChecker.
     * @param workingDir
     * @param ensembleController
     * @param ipcDir
     * @return
     */
    protected static ModelCheckingServerAbstract createModelCheckerFromConf(String confFile, String workingDir, WorkloadDriver ensembleController, String ipcDir) {
        Properties prop = new Properties();
        FileInputStream configInputStream = null;
        try {
            configInputStream = new FileInputStream(confFile);
            prop.load(configInputStream);
            configInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        String interceptorName = prop.getProperty("mc_name");
        int numNode = Integer.parseInt(prop.getProperty("num_node"));
        String testRecordDir = prop.getProperty("test_record_dir");
        String traversalRecordDir = prop.getProperty("traversal_record_dir");
        String strategy = prop.getProperty("exploring_strategy");
        int numCrash = Integer.parseInt(prop.getProperty("num_crash"));
        int numReboot = Integer.parseInt(prop.getProperty("num_reboot"));
        String initialPath = prop.getProperty("initial_path") != null ? prop.getProperty("initial_path") : "";
        String verifierName = prop.getProperty("verifier");
        String ackName = "Ack";

        SpecVerifier verifier = new FMCVerifier();
        ensembleController.setVerifier(verifier);

        FMCModelChecker modelCheckingServerAbstract = new FMCModelChecker(
                interceptorName, ackName, numNode, numCrash, numReboot, testRecordDir,
                traversalRecordDir, workingDir, ensembleController, ipcDir);

        modelCheckingServerAbstract.setInitialPath(initialPath);
        verifier.modelCheckingServer = modelCheckingServerAbstract;

        return modelCheckingServerAbstract;
    }

    /**
     *
     * @param checker
     * @param numNode
     * @param workingDir
     * @param pauseEveryPathExploration
     * @param ipcDir
     * @throws IOException
     */
    protected static void startExploreTesting(final ModelCheckingServerAbstract checker, int numNode, String workingDir, boolean pauseEveryPathExploration, String ipcDir) throws IOException {
        try {

            /**
             * This is where we record the paths we have explored, the main loop
             * will use these directory and flags to decide whether the checking
             * process goes on.
             */
            File gspathDir = new File(workingDir + "/record");

            int testId = gspathDir.list().length + 1;
            File finishedFlag = new File(workingDir + "/state/.finished");

            // This file will be created in FMCModelChecker.resetTest
            File waitingFlag = new File(workingDir + "/state/.waiting");

            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    checker.stopEnsemble();
                }
            });

            // activate Directory Watcher
            FileWatcher dirWatcher = new FileWatcher(ipcDir, checker);
            Thread watcherThread = new Thread(dirWatcher);
            watcherThread.start();
            Thread.sleep(300);

            for (; !finishedFlag.exists(); ++testId) {
                waitingFlag.delete();
                System.out.println("*** Path Exploration No-" + testId + " ***");

                checker.setTestId(testId);
                workloadDriver.resetTest(testId);
                /**
                 * If we don't add this statement, the transition hash_id will be
                 * different between two testId.
                 */
                dirWatcher.resetPacketCount();

                checker.runEnsemble();
                checker.waitOnSteadyStatesByTimeout();
                while (!waitingFlag.exists()) {
                    Thread.sleep(30);
                }
                checker.stopEnsemble();
                if (pauseEveryPathExploration) {
                    LOG.info("Press enter to continue...");
                    System.in.read();
//                    Integer a = System.in.read();
//                    while (a != 13){
//                        System.out.println(a);
//                        a = System.in.read();
//                    }
//                    System.out.println(a);
                } else {
                    // pause the next iteration for a while
                    Thread.sleep(200);
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
